function [numberedNodes, nodesAndEdges, nodeIndexMap, nodesNumbers] = ProcessNodesAndEdges(vectorOfNodes, l)
%   takes the vector containing all nodes and returns a cell that has all
%   nodes, and the 1/length (proportional to the conductances) adjacent to
%   each node.
%   a typical nodesAndEdges entry looks like this [i j g_i_j], which is
%   interpretted as the mutual conductance between ith and jth node.

    %%
    %finding all distinct nodes%
    nodesVec = {};
    for i = 1:size(vectorOfNodes, 2)
        info = vectorOfNodes{i};
        nodesVec{2*i - 1} = [info(1) info(2)];
        nodesVec{2*i} = [info(3) info(4)];
    end
    
    %nodesVec now have all the prospective distinct nodes%
    %need to eliminate the duplications%
    
    allNodes = cat(1,nodesVec{:});
    allNodes = unique(allNodes,'rows');
    
    allNodes = num2cell(allNodes,2);
    
    %%
    %number all nodes%    
    [nodeIndexMap, numberedNodes] = NumberNodes( allNodes, l );
    %%
    %now numberedNodes has all the nodes numbered%
    %now to compute the conductance of each node, and mutual conductance%
    
    %computing mutual conductances%
    mutualConductance = {};
    for i = 1:size(vectorOfNodes,2)
        info = vectorOfNodes{i};
        conductance = JunctionConductance(info);
        mutualConductance{i} = [info(1) info(2) info(3) info(4) conductance];
    end
    
    
    %%
    %now to get distinct nodes and their mutual conductances%
    for i = 1:size(mutualConductance,2)
        info = mutualConductance{i};
        firstPointVec{i} = [info(1) info(2)];
        secondPointVec{i} = [info(3) info(4)];
        condVec(i) = info(5);
    end
    
    %%
    %to get the numbers of nodes via the nodes' co-ordinates%
    nodesAndEdges = {};
    for i = 1:size(mutualConductance,2)
       firstNode = firstPointVec{i};
       secondNode = secondPointVec{i};
       
       firstNodeIndex = nodeIndexMap(sum(firstNode));
       secondNodeIndex = nodeIndexMap(sum(secondNode));
       
       firstNodeInfo = numberedNodes{firstNodeIndex};
       secondNodeInfo = numberedNodes{secondNodeIndex};
       
       if(size(firstNodeInfo,2) == 3) %no hash collision, just one node info stored at this index
           firstNodeNum = firstNodeInfo(1);
           
       else%there was a hash collision, and two nodes info stored
           for k = 1:(size(firstNodeInfo,2) / 3)
                position = 3*k - 1; %to get the position of the co-ordinates
                candidateNode = [firstNodeInfo(position) firstNodeInfo(position +1)];
                if(isequal(firstNode,candidateNode) == 1) %found the node
                    firstNodeNum = firstNodeInfo(position - 1);
                    break
                end
            end
       end
       
       if(size(secondNodeInfo,2) == 3) %no hash collision, just one node info stored at this index
           secondNodeNum = secondNodeInfo(1);
           
        else%there was a hash collision, and two nodes info stored
            for k = 1:(size(secondNodeInfo,2) / 3)
                position = 3*k - 1; %to get the position of the co-ordinates
                candidateNode = [secondNodeInfo(position) secondNodeInfo(position +1)];
                if(isequal(secondNode,candidateNode) == 1) %found the node
                    secondNodeNum = secondNodeInfo(position - 1);
                    break
                end
            end
       end
       
       nodesAndEdges{i} = [firstNodeNum secondNodeNum condVec(i)];
    end
    nodesNumbers = numberedNodes;
end

