function conductanceVec = ComputeConductance( numNodes, nodesAndConductanceVec )
% computes conduction vector for a mesh of resistors
    
    nodeNum = 1;
    count = 1; %to count the total number of intersections of the node
    conductanceVec = [];
    nodeInfo = {};
    for i = 1:size(nodesAndConductanceVec,2)
        %extract the connections of the current node%
        inform = nodesAndConductanceVec{i};
   
        %populate all intersections of the current node
        if(inform(1) == nodeNum) 
            nodeInfo{count} = inform;
            count = count + 1;
        
        else %gone onto a different node
            %nodeInfo has all the intersections of the current node%
            %computing the G entries
            nodesVec = MakeNodesVector(numNodes, nodeInfo);
            conductanceVec = [conductanceVec;nodesVec];
            
            nodeNum = inform(1);
            count = 1;
            nodeInfo = [];%resetting the edgeInter for new edge
            nodeInfo{count} = inform;
            count = count + 1;
        end
    end
    
    %for the last%
    nodesVec = MakeNodesVector(numNodes, nodeInfo);
    conductanceVec = [conductanceVec;nodesVec];

end

