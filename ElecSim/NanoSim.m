function [g1,g,G] = NanoSim(n,len,l,h,r)
%[mesh, intersections, nodesVec, nodes, nodesEdgesVec, nodesNum, allNodes, numNodesVec, G, g] = NanoSim(n,len,l,h,r)
%   ElecSim computes the electrical conductance of a mesh with n nanotubes.
%   l,h specify the length and height of the bounding box
%   n specifies the number of nanotubes to be simulated
%   r gives the ratio of the semiconducting nanotubes to n

    %%
    %construct edges%
    disp('forming the mesh');
    tic;
    mesh = ConstructNanotubes(n,len,l,h,r);
    toc;

    
        %%    
    %now to compute the intersections of the edges%
    disp('computing intersections');
    tic;
    [intersections, sourceDrainConnect] = ComputeNanotubesIntersections(mesh);
    toc;
    
    %%
    %getting the nodes%
    disp('compiling nodes')
    tic;
    interVec = num2cell(intersections,2)';
    [nodesVec, srcDrainConnect] = CompiledNodes(interVec, n); %compiling the nodes
    toc;
    %%
    %checking if there is no connection to source and drain electrodes: if
    %so then the conductance would be zero straight away%
    if(srcDrainConnect == 0) %no connection to either source or drain or both electrodes
        nodesVec = {};
        nodes = {};
        nodesEdgesVec = {};
        nodesNum = {};
        allNodes = {};
        conductanceVec = {};
        numNodesVec = {};
        G = [];
        g = 0;
        g1 = 0;
        return
    end
    
   
    %%
    %make adjustments for junctions%
    disp('accounting for junction nodes')
    tic;
    updatedInter = AdjustForJunctions(interVec,n,h);
    toc;
    
    %%
    %setting up the conductance matrix%
    disp('assembling the conductance matrix');
    tic;
    conductanceMat = SetUpConductanceMatrix(updatedInter);
    toc;
    %%
    %computing sheet conductance, i.e. conductance between first and last
    %nodes%
    disp('computing the sheet conductance');
    tic;
    g1 = ComputeSheetConductance(conductanceMat);
    toc;
   
    %%
    %to find the co-ordinate of the nodes%
    disp('finding nodes co-ordinates');
    tic;
    nodes = ComputeNanotubesNodes(nodesVec);
    toc;
    
    %%
    %setting up the nodes%
    disp('setting nodes and mutual conductances')
    tic;
    [nodesEdgesVec, totalNodesNum, allNodes, numNodesVec] = MutualNodeConductance(nodes, h);
    nodesNum = 0;
    if(totalNodesNum >= 2)
        nodesNum = totalNodesNum - 1;
    end
    numNodesVec = numNodesVec';
    toc;
    
    %arrange the nodesEdgesVec to optimize for speed%
    disp('arranging nodes');
    tic;
    nodesEdgesVec = ArrangeNodes(nodesEdgesVec);
    toc;
    
    %%
    %computing the conductance Matrix%
    
    disp('Computing sparse conductance matrix');
    tic;
    conductanceVec = ComputeConductance(nodesNum, nodesEdgesVec);
    toc;
    
    disp('setting up G');
    tic;
    G = [];
    if(isempty(conductanceVec) ~= 1)
        G = spconvert(conductanceVec);
    end
    toc;
    
    %%
    %computing the conductance, via solving GV = I%
    %I = G(t,t) - G(t,m) * (G(m,m) / G(m,t))
    disp('computing bulk conductance')
    tic;
    g = ComputeBulkConductance(G);
    toc;
end

