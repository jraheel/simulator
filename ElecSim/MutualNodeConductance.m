function [nodesEdgesVec, totalNodesNum, allNodes, numNodesVec, nodeIndexMap, numberedNodes] = MutualNodeConductance(nodes, l)
%   given the nodes cell, MutualConductance sets up the mutual conduction
%   between all the nodes

    nodesVec = {[]};
    for i = 1:size(nodes, 2)
        info = nodes{i};
        nodesVec{2*i - 1} = [info(1) info(2)];
        nodesVec{2*i} = [info(3) info(4)];
    end

    %nodesVec now have all the prospective nodes%
    %need to eliminate the duplications%
    
    allNodes = cat(1,nodesVec{:});
    allNodes = unique(allNodes,'rows');
    allNodes = num2cell(allNodes,2);
    
    %allNodes now have the list of all the nodes%
    
    
    indBottom = 1; indTop = 1; %to see how many nodes lie on the electrodes
    bottomNodes = {[]}; topNodes = {[]};
    for i = 1:length(allNodes)
        info = allNodes{i};
        if (info(2) == 0) %node is at the bottom electrode
            bottomNodes{indBottom} = info;
            indBottom = indBottom + 1;
            
        elseif (info(2) == l)%node is at the top electrode
            topNodes{indTop} = info;
            indTop = indTop + 1;
        end
    end
    
    %all nodes at the top electrodes are shorted, so they all are just one
    %nodes. Similarly all the nodes at the bottom electrode%
   
    totalNodesNum = length(allNodes) - length(bottomNodes) - length(topNodes) + 2;
    
    %%
    %getting all the nodes and edges%
    index = 1;
    vectorOfNodes = {};
    for i = 1:length(nodes)
        info = nodes{i};
        vectorOfNodes{index} = info;
        index = index + 1;
        if (info(2) ~= 0) %node does not have an edge with the bottom electrode
            vectorOfNodes{index} = [info(3) info(4) info(1) info(2) info(5)];
            index = index + 1;
        end
    end
    
    %vectorOfNodes now has all the desired nodes and edges%
    
    %switching all the nodes with the bottom electrode to be in the right
    %order
    
    for i = 1:size(vectorOfNodes,2)
        info = vectorOfNodes{i};
        if(info(2) == 0) %node with bottom electrode
            vectorOfNodes{i} = [info(3) info(4) info(1) info(2) info(5)];
        end
    end
    
    %%
    %Compute the mutual conductances of the modes%
    [numNodesVec, nodesEdgesVec, nodeIndexMap, numberedNodes] = ProcessNodesAndEdges(vectorOfNodes, l);
    
end

