function arrangedNodesEdgesVec = ArrangeNodes( nodesEdgesVec )
%ArrangeNodes takes a vector that has nodes information and returns a
%vector that has all the nodes arranged in ascending order
%   e.g. nodesEdgesVec = [1,2,0.5; 2,3,0.1; 1,3,1.25]. 
%   v = ArrangedNodes (nodesEdgesVec); v = [1,2,0.5;1,3,1.25;2,3,0.1].
    arrangedNodesEdgesVec = nodesEdgesVec;
    if(isempty(arrangedNodesEdgesVec) ~= 1)
        arrangedNodesEdgesVec = cat(1,arrangedNodesEdgesVec{:});
        arrangedNodesEdgesVec = sortrows(arrangedNodesEdgesVec,1);
        arrangedNodesEdgesVec = num2cell(arrangedNodesEdgesVec,2);
        arrangedNodesEdgesVec = arrangedNodesEdgesVec';
    end
end

