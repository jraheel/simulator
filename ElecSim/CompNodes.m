function nodesVec = CompNodes( interVec )
%	CompNodes compiles the number of nodes, formed via intersections of
%	two edges (resistors) as contained in the interVec.

%%
    %need to filter out the interVec for those edges which intersect less
    %than one edge% 
    
    edgeNum = 1; %to keep track of the current edge
    %an intersection point of an edge will only be a node if that edge
    %intersects more than once
    
    count = 1; %a counter
    nodeIndex = 1;
    singleIntEdges = {[]}; %need to disregard all the edges that intersect just once
    singleIntNum = 0;
    edgeInter = [];
    nodeInfo = {[]};
    for i = 1 : size(interVec, 2)
        info = interVec{i}; 
        
        %populate all intersections of the current edge
        if(info(1) == edgeNum) 
            edgeInter{count} = info;
            count = count + 1;
        
        else %gone onto a different edge
            %edgeInter has all the intersections of the current edge%
            
            if((length(edgeInter) ~= 1) && (isempty(edgeInter) ~= 1)) %more than one point of intersection
                for j = 1:size(edgeInter,2)
                    nodeInfo{nodeIndex} = edgeInter{j};
                    nodeIndex = nodeIndex + 1;
                end
                
            else %this edge has just one intersection
                singleIntEdges{singleIntNum + 1} = edgeNum;
                singleIntNum = singleIntNum + 1;
            end
            
            edgeNum = info(1);
            count = 1;
            edgeInter = [];%resetting the edgeInter for new edge
            edgeInter{count} = info;
            count = count + 1; 
        end
        
    end
    
    %for the last edge%
    if(length(edgeInter) ~= 1) %more than one point of intersection
        for j = 1:size(edgeInter,2)
            nodeInfo{nodeIndex} = edgeInter{j};
            nodeIndex = nodeIndex + 1;
        end
        
    else %this edge has just one intersection
        singleIntEdges{singleIntNum + 1} = edgeNum;
        singleIntNum = singleIntNum + 1;
    end 
    
    %now edgeNumToBeDeleted has all the edges that have just one
    %intersections points. All those other edges that intersect with these
    %one-intersection edges can not be nodes. So need to eliminate those
    %intersections from the nodeInfo
    if(isempty(singleIntEdges{1}) ~= 1) %there is one edge with just one intersection
        for i = 1:size(singleIntEdges,2)
            edgeToBeDel = singleIntEdges{i};
            
            for j = 1:size(nodeInfo,2)
                info = nodeInfo{j};
                if(isempty(info) ~= 1) %if info accessed an erased element of nodeInfo
                    if (info(2) == edgeToBeDel) %need to remove this intersection information
                        nodeInfo{j} = [];
                    end
                end
            end
        end
    end
    
    %%
    nodesVec = nodeInfo(~cellfun('isempty',nodeInfo));
    
end


