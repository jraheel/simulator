function b = IsPointInBox( point, box )
%checks if the point passed lies within the box or not
%   box = [xmin xmax ymin ymax], point = [x y]

    x = point(1);
    y = point(2);
    
    xmin = box(1);
    xmax = box(2);
    ymin = box(3);
    ymax = box(4);
    
    if ((xmin <= x) && (x <= xmax))
        if(((ymin <= y) && (y <= ymax)))
            b = 1;
        else
            b = 0;
        end
    else
        b = 0;
    end
end

