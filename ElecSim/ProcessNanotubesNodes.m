function nodes = ProcessNanotubesNodes( vec, nodesVec )
%   given the vec, n processes and returns the nodes

    %extract the co-ordinates of intersections%
    for i = 1:size(vec,2)
        inform = vec{i};
        interPoints(i,1) = inform(3);
        interPoints(i,2) = inform(4);
        interPoints(i,3) = inform(5);
    end
    
    n = sortrows(interPoints, 2); %sorting the nodes
    compiledNodes = nodesVec;
    if (isempty(compiledNodes) == 1) %nothing compiled yet
        ind = 0;
    
    else
        ind = size(compiledNodes, 2); %already processed nodes
    end
    
    for i = 1:(size(n,1) - 1)
        %the two nodes that connect directly via an edge%
        node1 = n(i,:);
        node2 = n(i+1,:);
        
        %node1(1) and node1(2)are respectively the x and y co-ordinates of
        %node 1, same for node2%
        
        compiledNodes{ind + i} = [node1(1) node1(2) node2(1) node2(2) node2(3)];
    end
    
    nodes = compiledNodes;
end


