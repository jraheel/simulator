function [intersections, sourceDrainConnect] = ComputeNanotubesIntersections( mesh )
%computes the intersections of the nanotubes in the mesh with each other
%and with the electrodes. 
%   a typical row in the matrix interserctions looks like [i j x0 y0 t].
%   This means ith nanotube intersects with jth nantoube at coordinate
%   (x0,y0) and t is either 0 or 1. If t is 1, this means that the ith and
%   jth nanotubes were of different types, i.e. one was metallic, the other
%   was semiconducting. If t = 0 then both ith and jth nanotubes were same
%   type.
    intersections = [];
    index = 1;
    n = size(mesh,1) - 2; %two electrodes
   
    %intersection with the source electrode%
    sourceDrainConnect = 0;
    connectionSourceElec = 0;
    connectionDrainElec = 0;
    
    sourceElectrode = mesh(n+2,:);
    sourceElectrode(5) = [];
    drainElectrode = mesh(n+1,:);
    drainElectrode(5) = [];
    yCoordinateSrc = sourceElectrode(2); %will be 'h'
    yCoordinateDrn = drainElectrode(2); %will be zero
   
     for i = 1:n
         tube = mesh(i,:);
         tube(5) = [];
         
         sourceInterPoint = intersectEdges (tube, sourceElectrode);
         if((isnan(sourceInterPoint(1)) ~= 1) && (isinf(sourceInterPoint(1)) ~= 1)) %edge intersects with source electrode
             intersections(index,:) = [i (n+2) sourceInterPoint(1) yCoordinateSrc 2];
             index = index + 1;
         end
         
         drainInterPoint = intersectEdges (tube, drainElectrode );
          if((isnan(drainInterPoint(1)) ~= 1) && (isinf(drainInterPoint(1)) ~= 1)) %edge intersects with drain electrode
             intersections(index,:) = [i (n+1) drainInterPoint(1) yCoordinateDrn 2];
             index = index + 1;
         end
     end
    
    for i = 1:n %do not want to store the intersections of electrodes
        tube1 = mesh(i,:);
        tube1type = tube1(5);
        tube1(5) = [];
        t = 5;
        for j = i + 1: n
            tube2 = mesh(j,:);
            tube2type = tube2(5);
            tube2(5) = [];
            
            %test if edges can intersect%
            Ax = tube1(1); Ay = tube1(2); Bx = tube1(3); By = tube1(4);
            Cx = tube2(1); Cy = tube2(2); Dx = tube2(3); Dy = tube2(4);
            
            det1 = ((Bx - Ax)*(Dy - Ay)) - ((Dx - Ax)*(By - Ay));
            det2 = ((Bx - Ax)*(Cy - Ay)) - ((Cx - Ax)*(By - Ay));
            
            if (det1 * det2 >= 0) %lines do not intersect
                point = [inf inf];
            else
                point = intersectEdges (tube1, tube2);
            end
            if((isnan(point(1)) ~= 1) && (isinf(point(1)) ~= 1)) %edges intersect
                if (tube1type == tube2type) %same types of nanotubes
                    t = 0;
                elseif (tube1type ~= tube2type)%different types of nanotubes intersecting
                    t = 1;
                end
                
                if(tube2type == 2) %intersecting an electrode
                    t = 2;
                end
                
                intersections(index,:) = [i j point(1) point(2) t];
                index = index + 1;
                
                intersections(index,:) = [j i point(1) point(2) t];
                index = index + 1;
  
            end 
        end
    end
    
    
    %if electrical connections to both source and drain electrodes have
    %been made%
    srcConnect = [];
    drnConnect = [];
    if(size(intersections,1) ~= 0)
        connections = intersections(:,2);
        srcConnect = find(connections == n+2, 1);
        drnConnect = find(connections == n+1, 1);
    end
    connectionSourceElec = ~(isempty(srcConnect));
    connectionDrainElec = ~(isempty(drnConnect));
    
    if((connectionSourceElec == 1) && (connectionDrainElec == 1))
        sourceDrainConnect = 1;
    end
    %sorting intersections via edge number (first column)
    if (size(intersections,1) ~= 0)
        intersections = sortrows(intersections,1);
    end
end

