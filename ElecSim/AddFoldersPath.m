function AddFoldersPath(path)
%adds the path to geom2d and other libraries. Path specifies the location
%to the root folder, i.e. where both ElecSim and geom2d will be present

    newPath = strcat(path,'/','geom2d');
    addpath(newPath);
    
    newPath = strcat(path,'/','RunSimulator');
    addpath(newPath);
    
    newPath = strcat(path,'/','ElecSim');
    addpath(newPath);
        
    cd(newPath);

end

