function g = ComputeBulkConductance(G)
%Computes conductance from the conductance matrix passed. It solves GV = I,
%for a unit potential applied across the source-drain electrodes
    
    n = size(G,1);  
    if(n == 0)
        g = 0;    
        return
        
    elseif(n == 1)
        g = G(1,1);
        return
    end
    
    if(any(G) == 0)%G is all zeros
        g = 0;
        return
    end
    
    %solve for Vm%
    Gm = G(2:n,2:n);
    vector = -G(2:n,1);
    %Vm = inv(Gm) * vector;
    Vm = Gm \ vector;

    Ge = G(1,2:n);
    g = G(1,1) + Ge * Vm;
end