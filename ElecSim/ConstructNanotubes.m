function [mesh, packingDensity] = ConstructNanotubes( n, fLen, l, h, r )
%   ConstructEdges forms 'n' cylindrical nanotubes at random. the dimensions 
%   of the bounding box, length and height that the nanotubes should lie 
%   within is specified by the box.
%
%   'r' specifies the ratio of semiconducting nanotubes. It should be
%   between 0 and 1, e.g. 0.33
%   
%   a nanotubes are of the form [x0 y0 x1 y1 t], where (x0,y0) and (x1,y1)
%   are end points of the rod, and t could be either 0 or 1 for metallic or
%   semi-conducting nanotube
%   The electrodes are also specified the same way, with t = 2

%need to create one edge that goes through the max length specified by
    
    %number of semiconducting tubes%
    numSemi = round(r * n);
    numMetallic = n - numSemi;
    
    box = [0 l 0 h];
    maxLen = distancePoints([0 0], [l h]); %maximum possible length
    mesh = [];
    packingDensity = 0; %to compute the number of nanotubes per unit area
    %first nanotube is picked randomly to be either a metallic or
    %semiconducting one
%     x1 = h * rand;
%     x2 = h * rand;
% 
%     rod = createEdge([x1 0], [x2 l]);
%     mesh(1,:) = [rod(1) rod(2) rod(3) rod(4)];

    %creating Semiconducting nanotubes%
    %len = 0.1 * maxLen; % a length of the nanotubes to be simulated. All of fixed length
    i = 1;
    while(size(mesh,1) < n)
        %generate center point%
        xc = maxLen * rand;
        yc = maxLen * rand;
        
        %generate an orientation of the edge%
        theta = pi * rand;
   
        %generate a length of the line%
%         len = fLen * rand;
%         s = len / 2;
        
        s = fLen / 2; % to be used to compute end point co-ordinates of the edges
        
        %find the co-ordinates of the end points%
        %initial points
        xi = xc - (s * sin(theta));
        yi = yc - (s * cos(theta));
        
        %end point
        xf = xc + (s * sin(theta));
        yf = yc + (s * cos(theta));
        
        %create the edge, and clip if it exceeds the bounds%
        e = createEdge([xi yi], [xf yf]);
        rod = clipEdge(e, box);
        
        d = distancePoints([rod(1) rod(2)],[rod(3) rod(4)]);
        if(d ~= 0)
            mesh(i,:) = [rod(1) rod(2) rod(3) rod(4)];
            packingDensity = packingDensity + d;
            i = i + 1;
        end
    end
    
    %%
    %to create semi-conducting and metallic tubes%
    nanotubeTypeVec = vertcat(ones(numSemi,1), zeros(numMetallic,1));
    mesh(:,size(mesh,2) + 1) = nanotubeTypeVec;
    
    %electrodes where the voltage shall be applied%
    mesh(n + 1, :) = [0 0 l 0 2]; %drain electrode
    mesh(n + 2, :) = [0 h l h 2]; %source electrode
    
    packingDensity = packingDensity / (l * h * fLen);
    %%
    close all
    PlotMesh(mesh,r);
end
