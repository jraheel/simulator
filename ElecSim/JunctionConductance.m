function conductance = JunctionConductance( info )
%returns the junction resistance between the two nanotubes passed
    JUNCTION_RESISTANCE = 2e5; %junction resistance between two nanotubes
    RESISTANCE_PER_UNIT_LENGTH = 13e3; %resistance per micron
    
    p1 = [info(1) info(2)];
    p2 = [info(3) info(4)];
    
    %resistance = (13k * length of the resistor)%
    %resistance = (distancePoints(p1, p2) * RESISTANCE_PER_UNIT_LENGTH) + JUNCTION_RESISTANCE; 
    resistance = (distancePoints(p1, p2) * RESISTANCE_PER_UNIT_LENGTH) + JUNCTION_RESISTANCE/2;
%     if(info(5) == 0)%junction with same type of nanotube or one of the electrodes    
%         conductance = 1/resistance;
%      elseif(info(5) == 1) %junction with a different type of nanotube
%          conductance = 0;
%     elseif(info(5) == 2)%junction between a nanotube and an electrode
%         resistance = (distancePoints(p1, p2) * RESISTANCE_PER_UNIT_LENGTH);
%         conductance = 1/resistance;
%     end
    
    conductance = 1/resistance;
end

