function arrangedNodes = MakeNodesVector( numNodes, nodeInfoVec )
%nodesVector computes the nodes and total mutual conductances of each node
    
    %arrange nodeInfoVec in ascending order of the intersected nodes%
    arrangedNodes = [];
    if(isempty(nodeInfoVec) == 1)
        return
    end
    
    nodeInfoVec = cat(1,nodeInfoVec{:});
    nodeInfoVec = sortrows(nodeInfoVec,2);
    nodeInfoVec = num2cell(nodeInfoVec,2);
    nodeInfoVec = nodeInfoVec';

    info = nodeInfoVec{1};
    currentNodeNum = info(2);
    conductanceVal = info(3);
    totalConductance = conductanceVal;
    index = 1;
    for i = 2:size(nodeInfoVec, 2)
        info = nodeInfoVec{i};
        if (info(2) == currentNodeNum) %same node
            conductanceVal = conductanceVal + info(3);
            totalConductance = totalConductance + info(3);
            
        else %different node reached
            if (currentNodeNum <= numNodes)
                arrangedNodes(index,:) = [info(1) currentNodeNum -conductanceVal];
            end
            currentNodeNum = info(2);
            conductanceVal = info(3);
            totalConductance = totalConductance + info(3);
            index = index + 1;
        end
    end
    
    %for last one%
    if (currentNodeNum <= numNodes)
        arrangedNodes(index,:) = [info(1) currentNodeNum -conductanceVal];
        
    else
        index = index - 1;
    end
    
    %computing total conductance for the row%
    arrangedNodes(index + 1,:) = [info(1) info(1) totalConductance];
    
    
end

