function [nodeIndexMap,numberedNodes] = NumberNodes( allNodes, h )
%numbers all nodes using a hashing algorithm, that simply adds the
%co-ordinates of each node in allNodes
    
    %%
    %to find the total number of distinct nodes%
    numTopNodes = 0; numBottomNodes = 0;
    
    for i = 1:length(allNodes)
        info = allNodes{i};
        
        if(info(2) == 0) %all nodes at the bottom electrode are first node
            numBottomNodes = numBottomNodes + 1;
        
        elseif (info(2) == h)
            numTopNodes = numTopNodes + 1;
        end
   
    end
    
    %total number of distinct nodes%
    distinctNodesNum = length(allNodes) - numBottomNodes - numTopNodes + 2;
    %%
    %now to number those nodes%
    nodeIndexMap = containers.Map(1.25,1); %to create a map with correct keyType and ValueType
    remove(nodeIndexMap,1.25); %deleting the junk initial value from the map
    nodeNum = 1;
    index = 1; %for the index of the cell array for nodes
    nodeIndex = 2;
    numberedNodes = {};
    keyExists = 0; %to check if the key generated already exists 
    for i = 1:size(allNodes,1)
        info = allNodes{i};
        key = sum(info);
        
        %check if this key already exists in the map%
        if(isKey(nodeIndexMap,key) == 0) %this key doesn't exist
            keyExists = 0;
            nodeIndexMap(key) = index;
            index = index + 1;
        else
            keyExists = 1;
        end
        
        %create a cell array to store the node co-ordinates and assign a
        %node number to this node%
        
        %assign a number to this node%
        if (info(2) == h)%node at the top electrode
            nodeNum = 1;
        elseif(info(2) == 0)%node at the bottom electrode
            nodeNum = distinctNodesNum;
        else
            nodeNum = nodeIndex;
            nodeIndex = nodeIndex + 1;
        end
        
        if (keyExists == 0)
            numberedNodes{index - 1} = [nodeNum info(1) info(2)];
        
        else %there already is some data stored at this index
            data = numberedNodes{index - 1};
            newData = [nodeNum info(1) info(2)];
            numberedNodes{index - 1} = horzcat(data, newData);
        end
    end
    numberedNodes = numberedNodes';
end

