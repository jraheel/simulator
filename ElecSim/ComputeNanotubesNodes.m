function nodes = ComputeNanotubesNodes( nodesVec )
%	ComputeNodes computes the number of nodes, formed via intersections of
%	two edges (resistors) as contained in the nodesVec.


%   nodes is a vector that contains the co-ordinates of the intersections
%   i.e. [x0 y0 x1 y1 t]. This means point (x0,y0) is a node and it
%   intersects with point (x1,y1). t is either 0 or 1, and denotes if the
%   junction at (x1,y1) is between a metallic and a semiconducting tube or
%   between same type of nanotubes.

%%
%   edgeNum = 1; %first edge will definitely have more than one intersection points
    count = 1; %to count the total number of intersections of the edge
    nodesVector = {};
    if (isempty(nodesVec) ~= 1)
        inform = nodesVec{1};
        edgeNum = inform(1);
        for i = 1:size(nodesVec,2)
            %extract the intersections of the current edge
            inform = nodesVec{i};

            %populate all intersections of the current edge
            if(inform(1) == edgeNum) 
                edgeInter{count} = inform;
                count = count + 1;

            else %gone onto a different edge
                %edgeInter has all the intersections of the current edge%
                %processing the nodes
                if((length(edgeInter) ~= 1) && (isempty(edgeInter) ~= 1)) %just one point of intersection
                    nodesVector = ProcessNanotubesNodes(edgeInter, nodesVector);
                end
                edgeNum = inform(1);
                count = 1;
                edgeInter = [];%resetting the edgeInter for new edge
                edgeInter{count} = inform;
                count = count + 1;
            end
        end


        %for the last edge%
        if(length(edgeInter) ~= 1) %just one point of intersection
            nodesVector = ProcessNanotubesNodes(edgeInter, nodesVector);
        end
    end
    nodes = nodesVector;
end


