function PlotMesh( mesh, ratio )
%plots the mesh on a plot. Electrodes in red, metallic nanotubes in blue
%and semiconducting nanotubes in black
    [r,c] = size(mesh);
    meshMat = mesh;
    meshMat(:,c) = []; %deleting the last column that specifies nanotube type
    
    n = size(mesh,1);
    n = n - 2;%2 electrodes
    
    %draw the electrodes%
    drawEdge(meshMat(n+1,:),'Color','red','LineWidth',5);
    hold on;
    drawEdge(meshMat(n+2,:),'Color','red','LineWidth',5);
    
    numSemi = round(ratio * n);
    for i = 1:numSemi
        drawEdge(meshMat(i,:),'Color','m','LineWidth',1);
    end
    
    for i = (numSemi + 1):n
        drawEdge(meshMat(i,:),'Color','k','LineWidth',1);
    end
    
end

