function G = ComputeConductanceMatrix( numNodes, nodesAndConductanceVec )
%   computes conduction matrix for a mesh of resistors

G = zeros(numNodes); %one node is the reference node, considered ground

for i = 1:size(G,1)
        for j = 1:size(G,2)
            
            if(i == j) %diagonal entry
                val = 0;
                for k = 1: size(nodesAndConductanceVec,2)
                    info = nodesAndConductanceVec{k};
                    if (info(1) == i) %an edge that connects to this node
                        val = val + info(3);
                    end
                end
                G(i,j) = val;
            
            elseif(i ~= j) %an off diagonal entry
                val = 0;
                for k = 1: size(nodesAndConductanceVec,2)
                    info = nodesAndConductanceVec{k};
                    if((info(1) == i) && (info(2) == j))
                      val = val + info(3);
                    end
                end
                G(i,j) = -val;
            end
        end
    end

end

