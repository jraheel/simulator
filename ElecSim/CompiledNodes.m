function [compNodesVec, sourceDrainConnect] = CompiledNodes( interVec, n )
%   calls CompNodes function repeatedly with all the intersections to
%   identify the edges that intersect more than once for a mesh

    nodesVec = CompNodes(interVec);
    newNodesVec = CompNodes (nodesVec);
    
    while(isequal(cell2mat(nodesVec), cell2mat(newNodesVec)) == 0)
            nodesVec = newNodesVec;
            newNodesVec = CompNodes(nodesVec);
    end
    compNodesVec = newNodesVec;
    
    if(isempty(compNodesVec) == 1)
        sourceDrainConnect = false;
        return
    end
    %%
    %now to check if there still is a connection with the source and drain
    %electrodes
    compNodesMat = cat(1,compNodesVec{:});
    
    intersectionVec = compNodesMat(:,2); %extracting second row to see if there are edges intersecting with the electrodes
    sourceDrainConnect = true;
    
    indexDrain = find(intersectionVec == n+1, 1);
    indexSource = find(intersectionVec == n+2, 1);
    
    if((isempty(indexSource) == 1) || ((isempty(indexDrain) == 1)))
        sourceDrainConnect = false;
    end
end

