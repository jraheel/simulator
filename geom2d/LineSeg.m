function [x,y] = LineSeg( x1,y1, x2,y2 )
%takes two co-ordinates, (x1,y1) & (x2,y2) and returns two vectors, x & y
%where corresponding values (x(i),y(i)) are co-ordinates of the line
%segment between the points (x1,y1) & (x2,y2)

if (x2 < x1) 
    tx = x1;
    x1 = x2;
    x2 = tx;
    
    ty = y1;
    y1 = y2;
    y2 = ty;
end

x = [x1:0.0001:x2]; % create the vector x

% y = mx + c, need to compute m and c
m = (y2 - y1) / (x2 - x1);
c = y1 - (m * x1);

y = (m * x) + c;
end

