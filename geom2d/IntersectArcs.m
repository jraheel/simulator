function intersections = IntersectArcs( arc1, arc2, epsilon, box )
% computes the intersection points of the two arcs passed. If no
% intersection, then returns an empty array. l specifies the box of
% interest

    %create circles from arcs
    c1 = [arc1(1) arc1(2) arc1(3)];
    c2 = [arc2(1) arc2(2) arc2(3)];
    intersections = [];
    intersectionPoints = intersectCircles (c1, c2);
    intersectionPoints = unique(intersectionPoints,'rows');
    index = 1;
   
    %now to check if these points lie on the arcs as well%
    for i = 1:size(intersectionPoints,2)
        point = intersectionPoints(i,:);
        if(isnan(point(1)) == 0) %an intersection point
            %to see if the point lies on the two arcs
            if(IsPointOnArc(arc1,point,epsilon) == 1) && (IsPointOnArc(arc2,point,epsilon) == 1)
               if(IsPointInBox(point,box) == 1)
                    intersections(index,:) = point;
                    index = index + 1;
               end
            end
        end
    end
end

