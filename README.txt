Main script: Simulate.m
function: I = Simulate(n,l,volts)

input arguments:'n' is the number of nanotubes to be thrown at random. 
		'l' specifies the dimension of plane x,y belong to [0,l]. It is implemented dimensionless.
		'volts' are the applied voltage across the electrodes. Implemented dimensionless, but interpreted in microvolts

e.g. I = Simulate(100, 25, 5);
Simulate will set up a random mesh of 100 carbon nanotubes on a plane of diagonal ends (0,0) and (25,25).
The resulting mesh's conductance matrix,G will be computed. 
Finally, GV = I shall be solved.
